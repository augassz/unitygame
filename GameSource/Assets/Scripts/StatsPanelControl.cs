﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class StatsPanelControl : MonoBehaviour {

    PlayerStats player;

    Text[] textObjects;

    // Use this for initialization
    void Start () {

        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        textObjects = gameObject.GetComponentsInChildren<Text>();
    }
	
	// Update is called once per frame
	void Update () {
            ShowStats();
    }

    void ShowStats() {
        string text = "";
        float num = 0.0f;
        string result = "";
        float toNextLv = 0.0f;

        foreach (Text t in textObjects) {
            if(t.name == "level") {
                text = "Level:";
                num = player.GetLevel();
                result = string.Format("{0,-10}   {1,5}", text, Math.Round(num, 2));
            }
            else if(t.name == "exp") {
                text = "Exp:";
                num = player.GetExp();
                toNextLv = player.GetExpToNext();
                result = string.Format("{0,-10}   {1,5}/{2,-5}", text, num.ToString(), toNextLv.ToString());
            }
            else if(t.name == "hpRegen") {
                text = "Hp. regen:";
                num = player.GetHpRegen();
                result = string.Format("{0,-10}   {1,5}", text, Math.Round(num, 2));
            }
            else if(t.name == "hp") {
                text = "Hp:";
                num = player.GetHp();
                float num2 = player.GetMaxHp();
                result = string.Format("{0,-10}   {1,5}/{2,-5}", text, num, num2);

            }
            else if(t.name == "def") {
                text = "Defence:";
                num = player.GetDef();
                result = string.Format("{0,-10}   {1,5}", text, Math.Round(num, 2));
            }
            else if(t.name == "droprate") {
                text = "Drop rate:";
                num = player.GetDropRate();
                result = string.Format("{0,-10}   {1,5}", text, Math.Round(num, 2));
            }
            else if(t.name == "movespeed") {
                text = "Speed:";
                num = player.GetMoveSpeed();
                result = string.Format("{0,-10}   {1,5}", text, Math.Round(num, 2));
            }
            else if(t.name == "jumpheight") {
                text = "Jump:";
                num = player.GetJumpH();
                result = string.Format("{0,-10}   {1,5}", text, Math.Round(num, 2));
            }
            else if(t.name == "critrate") {
                text = "Crit. Rate:";
                num = player.GetCritRate();
                result = string.Format("{0,-10}   {1,5}", text, Math.Round(num, 2));
            }
            else if (t.name == "atk") {
                text = "Atk. dmg:"; 
                num = player.GetAtkDmg();
                result = string.Format("{0,-10}   {1,5}", text, Math.Round(num, 2));
            }
            else if (t.name == "statpoints")
            {
                text = "Stat Pts. left:";
                num = player.GetStatPoints();
                result = string.Format("{0,-10}   {1,5}", text, num);
            }
            else if (t.name == "str")
            {
                text = "STR:";
                num = player.GetStr();
                result = string.Format("{0,-10}   {1,5}", text, num);
            }
            else if (t.name == "con")
            {
                text = "CON:";
                num = player.GetCons();
                result = string.Format("{0,-10}   {1,5}", text, num);
            }
            else if (t.name == "dex")
            {
                text = "DEX:";
                num = player.GetDex();
                result = string.Format("{0,-10}   {1,5}", text, num);
            }
            else if (t.name == "luck")
            {
                text = "LUCK:";
                num = player.GetLuck();
                result = string.Format("{0,-10}   {1,5}", text, num);
            }
            else
            { result = t.text; }

            t.text = result;
        }
    }

}
