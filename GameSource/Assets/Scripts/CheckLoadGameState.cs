﻿using UnityEngine;
using System.Collections;

public class CheckLoadGameState : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if(PlayerPrefs.GetInt("loadgame") == 0)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
        }
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
