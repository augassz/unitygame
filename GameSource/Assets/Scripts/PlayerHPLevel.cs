﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHPLevel : MonoBehaviour {

    PlayerStats player;

    Text[] textObjects;
    Text level;
    Text statspts;

    Slider[] sliderObjs;
    Slider hpSlider;
    Slider expSlider;

    float nextRecalc;

    GameObject scoreGameObject;
    GameObject mapLevelGameObject;

    Text score;
    Text mapLevel;
   

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();

        textObjects = gameObject.GetComponentsInChildren<Text>();

        foreach (Text t in textObjects){
            if(t.name == "Lv"){
                level = t;
            }
            else if(t.name == "Stats") {
                statspts = t;
            }
        }

        sliderObjs = gameObject.GetComponentsInChildren<Slider>();

        foreach (Slider s in sliderObjs)
        {
            if (s.name == "HpBar")
            {
                hpSlider = s;
            }
            else if (s.name == "ExpBar")
            {
                expSlider = s;
            }
        }

        scoreGameObject = GameObject.Find("ScoreTEXT");
        mapLevelGameObject = GameObject.Find("MapLevelTEXT");

        score = scoreGameObject.GetComponent<Text>();
        mapLevel = mapLevelGameObject.GetComponent<Text>();
    }

	// Update is called once per frame
	void Update () {
        
            RecalcHP();
            RecalcLevel();
            RecalcExp();
            RecalcStats();
            RecalcScore();
        
	
	}

    void RecalcScore() {

        string text = "";
        float num = 0;
        string result = "";

        text = "Score:";
        num = player.GetScore();

        result = string.Format("{0,-7}   {1,-10}", text, num);
        score.text = result;

        text = "Map Level:";
        num = player.GetMapLevel();

        result = string.Format("{0,-10}   {1,-5}", text, num);
        mapLevel.text = result;
    }

    void RecalcHP() {
        float hpValue = player.GetHp() / player.GetMaxHp() * 100;
        hpSlider.value = hpValue;

    }

    void RecalcExp() {
        float expValue = player.GetExp() / player.GetExpToNext() * 100;
        expSlider.value = expValue;
    }

    void RecalcLevel() {

        string text = "";
        int num = 0;
        string result = "";

        text = "Level:";
        num = player.GetLevel();

        result = string.Format("{0,-7}   {1,-5}", text, num);
        level.text = result;

    }

    void RecalcStats() {

        string text = "";
        int num = 0;
        string result = "";

        text = "Stat pts:";
        num = player.GetStatPoints();

        result = string.Format("{0,-10}   {1,-5}", text, num);
        statspts.text = result;

    }



}
