﻿using UnityEngine;
using System.Collections;

public class fps : MonoBehaviour {

    float deltaTime = 0.0f;
    bool showfps;
    void Awake()
    {
        if (PlayerPrefs.GetInt("showfps") == 1)
            showfps = true;
        else
            showfps = false;
    }

    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;

    }

    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(80, 10, 0, 0);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
        if(showfps)
             GUI.Label(rect, text, style);
    }
}
