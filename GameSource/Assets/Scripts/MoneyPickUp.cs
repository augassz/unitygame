﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoneyPickUp : MonoBehaviour {

    PlayerStats player;
    int value;
    bool moneyGived;
    Text txt;
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        value = Random.Range(player.GetMapLevel(), player.GetMapLevel() + 5) * 10;
        moneyGived = false;
        txt = gameObject.GetComponentInChildren<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        Rotate();
    }

    void Rotate()
    {
        transform.RotateAround(transform.position, transform.up, Time.deltaTime * 50.0f);
    }

    void OnTriggerEnter(Collider c)
    {
        if(c.gameObject.tag == "Player")
        {
            if (!moneyGived)
            {
                player.SetGold(player.GetGold() + value);
                moneyGived = true;
                txt.text = "+" + value + " money";
                Destroy(gameObject, 1.2f);
            }
                
        }
    }
}
