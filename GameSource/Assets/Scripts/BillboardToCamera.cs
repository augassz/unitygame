﻿using UnityEngine;
using System.Collections;

public class BillboardToCamera : MonoBehaviour {

    private Camera main_Camera;

	// Use this for initialization
	void Start () {
        GameObject cam = GameObject.FindGameObjectWithTag("MainCamera");
        main_Camera = cam.GetComponent<Camera>();

    }
	// Update is called once per frame
	void Update () {
        transform.LookAt(transform.position + main_Camera.transform.rotation * Vector3.forward, main_Camera.transform.rotation * Vector3.up);
    }
}
