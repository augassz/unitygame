﻿using UnityEngine;
using System.Collections;

public class CloseEntrWall : MonoBehaviour {

	GameObject player;
	bool lower;

	// Use this for initialization
	void Start () {
		lower = false;
        player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.x - player.transform.position.x <= -5.0f) { 
			lower = true;
        }

        if (lower == true && transform.position.y > 5.0f)
            transform.position = new Vector3(transform.position.x, transform.position.y - Time.deltaTime * 3, transform.position.z);

    }
}
