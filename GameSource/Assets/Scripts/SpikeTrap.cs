﻿using UnityEngine;
using System.Collections;

public class SpikeTrap : MonoBehaviour {

    PlayerStats player;
    Transform spikes;
    float timeInteral;
    float dmg;
    bool extend;
    bool invoked;
    AudioSource audiosource;
    public AudioClip thrust;
    bool played = false;

    // Use this for initialization
    void Start () {
        audiosource = gameObject.GetComponent<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        spikes = gameObject.GetComponentInChildren<Transform>();
        timeInteral = Random.Range(3.0f, 5.0f);
        dmg = Random.Range(player.GetDef(), player.GetDef() + 30.0f);
        extend = true;
        invoked = false;
    }
	
	// Update is called once per frame
	void Update () {
        
        if (extend)
        {
            Extend();
            
        }
        else
        {
            Contract();
        }
	
	}

    void OnTriggerEnter(Collider c)
    {
        if(spikes.transform.position.y >= -1.0f)
        {
            if (c.gameObject.tag == "Player")
                player.GetDamaged(dmg);
        }
    }

    void Extend()
    {
        
        if (spikes.position.y >= 1.2f)
        {
            if (!played)
            {
                audiosource.PlayOneShot(thrust);
                played = true;
            }
            Invoke("extendd", 3.0f);
        }
        else if(spikes.position.y <= 1.5f)
            spikes.position += spikes.up * Time.deltaTime * 10.0f;
    }
    void extendd()
    {
        played = false;
        extend = false;
    }

    void Contract()
    {
        if (spikes.position.y <= -2.0f)
        {
            if (!invoked)
            {
                Invoke("changeExtend", timeInteral);
                invoked = true;
            }
        }
        else if(spikes.position.y >= -2.5f)
            spikes.position -= spikes.up * Time.deltaTime * 2.0f;
    }
    void changeExtend()
    {
        extend = true;
        invoked = false;
    }
}
