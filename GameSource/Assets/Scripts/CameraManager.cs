﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

	GameObject player;
	float smooth = 3.5f;
	float newCameraPos;
	float defaultCameraPos;
	//private float mapGenStep = 100.0f;
	float cameraBackLimit = 105.0f;

	private const int mMessageWidth  = 200;
	private const int mMessageHeight = 64;

    AudioListener audioListener;

	// Use this for initialization
	void Start () {
        audioListener = gameObject.GetComponent<AudioListener>();
        player = GameObject.FindGameObjectWithTag ("Player");
		defaultCameraPos = transform.position.x;

        if(PlayerPrefs.GetInt("audio") == 1)
        {
            AudioListener.volume = 1.0f;
        }
        else if(PlayerPrefs.GetInt("audio") == 0)
        {
            AudioListener.volume = 0.0f;

        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {

		if (player.transform.position.x - transform.position.x >= 10.0f) {
			newCameraPos = transform.position.x + 20.0f;
			if (newCameraPos > cameraBackLimit) {
                player.GetComponent<PlayerStats>().SetMapLevel(1);
                cameraBackLimit += 100.0f;
				defaultCameraPos += 100.0f;
			}
		}
		if(player.transform.position.x - transform.position.x <= -10.0f){
			newCameraPos = transform.position.x - 20.0f;
		}
		if (newCameraPos <= defaultCameraPos - 5.0f)
			newCameraPos = defaultCameraPos;
		transform.position = Vector3.Lerp (transform.position, new Vector3 (newCameraPos, transform.position.y, transform.position.z), smooth * Time.deltaTime);

	}

}
