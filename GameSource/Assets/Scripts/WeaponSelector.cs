﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class WeaponSelector : MonoBehaviour {

    List<GameObject> weapons;
    Dropdown dropdown;

	// Use this for initialization
	void Start () {
        weapons = new List<GameObject>();
        dropdown = gameObject.GetComponent<Dropdown>();
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Weapon"))
        {
            weapons.Add(g);
            dropdown.options.Add(new Dropdown.OptionData(g.name));
            g.SetActive(false);
        }
        dropdown.value = 0;
        weapons[0].SetActive(true);
    }

    public void ChangeWeapon()
    {
        foreach(GameObject g in weapons)
        {
            g.SetActive(false);
        }
        weapons[dropdown.value].SetActive(true);
    }
}
