﻿using UnityEngine;
using System.Collections;

public class DetectCollision : MonoBehaviour {

    Collider colliderEnemy;
    Collider colliderPlayer;

    // Use this for initialization
    void Start () {

    }

    void Update () {

     
    }

    public Collider GetColliderEnemy()
    {
        return colliderEnemy;
    }

    public Collider GetColliderPlayer()
    {
        return colliderPlayer;
    }

    void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            colliderEnemy = other;
        }
        else if (other.gameObject.tag == "Player")
        {
            colliderPlayer = other;
        }
        else if(other.gameObject.tag == "Chest")
        {
            colliderEnemy = other;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            colliderEnemy = null;
        }
        else if (other.gameObject.tag == "Player")
        {
            colliderPlayer = null;
        }          
    }

}
