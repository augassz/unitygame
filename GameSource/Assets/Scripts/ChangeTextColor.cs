﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeTextColor : MonoBehaviour {

    bool onOff;

	// Use this for initialization
	void Start () {
        if(gameObject.name == "OptionSoundText")
        {
            if(PlayerPrefs.GetInt("audio") == 1)
                gameObject.GetComponent<Text>().color = Color.green;
            else
                gameObject.GetComponent<Text>().color = Color.black;
        }
        else if(gameObject.name == "OptionFpsText")
        {
            if (PlayerPrefs.GetInt("showfps") == 1)
                gameObject.GetComponent<Text>().color = Color.green;
            else
                gameObject.GetComponent<Text>().color = Color.black;
        }
    }

    public void ChangeColor()
    {
        if (gameObject.GetComponent<Text>().color == Color.green)
        {
            gameObject.GetComponent<Text>().color = Color.black;
        }
        else if (gameObject.GetComponent<Text>().color == Color.black)
        {
            gameObject.GetComponent<Text>().color = Color.green;
        }
    }
	
	void Update () {
	
	}
}
