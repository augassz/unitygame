﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyHPBar : MonoBehaviour {

    EnemyStats enemyStats;

    Slider hpSlider;
    Text level;

    // Use this for initialization
    void Start () {
        enemyStats = gameObject.GetComponentInParent<EnemyStats>();
        hpSlider = gameObject.GetComponentInChildren<Slider>();
        level = gameObject.GetComponentInChildren<Text>();
    }
	
	// Update is called once per frame
	void Update () {

       RecalcHP();
        
    }

    void RecalcHP()
    {
        float hpValue = enemyStats.GetHp() / enemyStats.GetMaxHp() * 100;
        hpSlider.value = hpValue;

        string text = "";
        int num = 0;
        string result = "";

        text = "Lv.";
        num = enemyStats.GetLevel();

        result = string.Format("{0,-3}   {1,4}", text, num);
        level.text = result;
    }
}
