﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

    GameObject player;
    EnemyStats enemyStats;
    Animator animator;
    Vector3 spawnPos;
    Vector3 newPos;
    bool atNewPos;

    int behav;
    float alertDist;
    bool alertBool;
    bool attacked;
    float attackTimer;
    bool invoked;
    bool canMove;
    Vector3 lastPos;

    // Use this for initialization
    void Start() {

        animator = gameObject.GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        enemyStats = gameObject.GetComponent<EnemyStats>();
        behav = enemyStats.GetBehaviour();
        alertDist = enemyStats.GetAlertDist();

        spawnPos = new Vector3(transform.position.x, 1.0f, transform.position.z);
        newPos = spawnPos;
        atNewPos = false;
        alertBool = false;
        attacked = false;
        canMove = true;
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (!enemyStats.IsDying()) {

            lastPos = gameObject.transform.position;
            newPos.y = transform.position.y;

            if (Vector3.Distance(player.transform.position, transform.position) <= alertDist) // Player spotted
            {
                alertBool = true;
            }
            else if (Vector3.Distance(player.transform.position, transform.position) > alertDist)
            {
                alertBool = false;
            }



            if (behav == 0) // Stupid
            {
                if (alertBool && Vector3.Distance(player.transform.position, transform.position) <= 6.0f)
                {
                    if (Vector3.Distance(player.transform.position, transform.position) >= 3.0f)
                        Move(player.transform.position);
                    Attack();
                }

            }
            else if (behav == 1)    // Berserk
            {
                if (!alertBool)
                    Idle();
                else if (alertBool && Vector3.Distance(player.transform.position, transform.position) <= 3.0f)
                {
                    Attack();
                }
                else
                    Move(player.transform.position);

            }
            else if (behav == 2)    // Hit and run
            {
                if (!alertBool)
                    Idle();
                else if (alertBool && Vector3.Distance(player.transform.position, transform.position) <= 3.0f && !attacked)
                {
                    Attack();
                    attacked = true;
                    BackPos();
                    alertDist = 20.0f;
                    canMove = false;
                    Invoke("LetMove", 1.0f);
                }
                else if (!attacked && canMove)
                    Move(player.transform.position);
                else if (attacked && canMove)
                {
                    Move(newPos);
                    if (newPos.x - transform.position.x <= 1.0f && newPos.z - transform.position.z <= 1.0f)
                        attacked = false;
                }
            }

            if (lastPos.x > gameObject.transform.position.x || lastPos.x < gameObject.transform.position.x)
                animator.SetBool("Walk", true);
            else
                animator.SetBool("Walk", false);
        }
        


        if (player.transform.position.x - transform.position.x > 110.0f) // Die if player is far away
        {
            Delete();
        }
    }

    void Idle()
    {
        if (newPos.x - transform.position.x <= 1.0f && newPos.z - transform.position.z <= 1.0f)
        {
            atNewPos = true;
            canMove = false;
        }
        if (atNewPos && !alertBool)
        {
            CreateNewPos();
            atNewPos = false;
            Invoke("LetMove", Random.Range(3.0f, 7.0f));
        }

        Move(newPos);
    }

    void Attack()
    {
        gameObject.transform.LookAt(new Vector3(player.transform.position.x, player.transform.position.y + 0.5f, player.transform.position.z));
        if (attackTimer <= Time.time)
        {
            animator.SetTrigger("Attack");

            Collider c = gameObject.GetComponentInChildren<DetectCollision>().GetColliderPlayer();
            if (c != null)
            {
                c.gameObject.GetComponent<PlayerStats>().GetDamaged(enemyStats.DoDamage());
            }

            attackTimer = Time.time + Random.Range(1.0f, 5.0f);
        }
        
    }

    void BackPos()
    {
        if (transform.position.x <= player.transform.position.x)
            newPos.x = transform.position.x - Random.Range(6.0f, 12.0f);
        else if (transform.position.x > player.transform.position.x)
            newPos.x = transform.position.x + Random.Range(6.0f, 12.0f);
    }

    void Move(Vector3 target)
    {
        
        if (canMove)
        {
            if (alertBool)
            {
                gameObject.transform.LookAt(target);
                gameObject.transform.position += transform.forward * Time.deltaTime * enemyStats.GetMoveSpeed();
            }
            else
            {
                gameObject.transform.LookAt(target);
                gameObject.transform.position += transform.forward * Time.deltaTime * (enemyStats.GetMoveSpeed() / 3.0f);
            }
        }
    }
    void LetMove()
    {
        canMove = true;
    }

    void  CreateNewPos()
    {
        newPos.x = Random.Range(transform.position.x - 20.0f, transform.position.x + 20.0f);
        newPos.z = Random.Range(1.0f, 9.0f);
    }

	void Delete(){
		Destroy (gameObject);
	} 
}
