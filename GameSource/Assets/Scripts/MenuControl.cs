﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System;
using System.Collections.Generic;

public class MenuControl : MonoBehaviour {

    public GameObject Canvases;
    private Canvas[] cnvs;

    private Canvas MainMenuCanvas;
    private Canvas OptionsCanvas;
    private Canvas HighscoresCanvas;
    private Canvas AboutCanvas;

    void Awake() {
        cnvs = new Canvas[5];
        cnvs = Canvases.GetComponentsInChildren<Canvas>();
        for(int i = 0; i < cnvs.Length; i++){
            if(cnvs[i].name == "MainMenuCanvas") {
                MainMenuCanvas = cnvs[i];
            }
            else if(cnvs[i].name == "OptionsMenuCanvas") {
                OptionsCanvas = cnvs[i];
            }
            else if (cnvs[i].name == "HighscoresCanvas") {
                HighscoresCanvas = cnvs[i];
            }
            else if (cnvs[i].name == "AboutCanvas") {
                AboutCanvas = cnvs[i];
            }

        }

        OptionsCanvas.enabled = false;
        HighscoresCanvas.enabled = false;
        AboutCanvas.enabled = false;
    }

    public void StartGame() {
        PlayerPrefs.SetInt("gamestart", 0);
        PlayerPrefs.Save();
        SceneManager.LoadScene(1);
    }

    public void LoadGame()
    {
        if(PlayerPrefs.GetInt("loadgame") == 0)
        {
            Debug.Log("There is no saved game.");
        }
        else
        {
            PlayerPrefs.SetInt("gamestart", 1);
            PlayerPrefs.Save();
            SceneManager.LoadScene(1);
        }
        
    }


    public void OptionsMenu() {
        OptionsCanvas.enabled = true;
        MainMenuCanvas.enabled = false;
    }

    struct scoreStruct
    {
        public string text; 
        public int score;
    }

    public void HighscoreMenu() {
        HighscoresCanvas.enabled = true;
        MainMenuCanvas.enabled = false;

        StreamReader reader = new StreamReader(Application.persistentDataPath + "/Highscores.txt");
        String highscores = "";

        List<scoreStruct> list = new List<scoreStruct>();
        while (!reader.EndOfStream)
        {
            scoreStruct s = new scoreStruct();

            string[] strs = new string[2];
            string line = reader.ReadLine();
            strs = line.Split('-');

            s.text = strs[0];
            s.score = int.Parse(strs[1]);

            list.Add(s);
        }
        reader.Close();

        list.Sort((s1, s2) => s1.score.CompareTo(s2.score));
        list.Reverse();
        for(int i = 0; i < list.Count; i++)
        {
            highscores += (i+1).ToString() + ".   " + list[i].text + "   " + list[i].score.ToString() + "@";
            if (i == 9)
                break;
        }

        highscores = highscores.Replace("@", "" + System.Environment.NewLine);

        GameObject text = GameObject.Find("scoretekstas");
        text.GetComponent<Text>().text = highscores;
        list.Clear();
    }

    public void AboutMenu() {
        AboutCanvas.enabled = true;
        MainMenuCanvas.enabled = false;
    }

    public void ReturnToMenu() {
        OptionsCanvas.enabled = false;
        HighscoresCanvas.enabled = false;
        AboutCanvas.enabled = false;
        MainMenuCanvas.enabled = true;
    }

    public void MuteSound()
    {
        if(PlayerPrefs.GetInt("audio") == 0)
        {
            GameObject.Find("OptionSoundText").GetComponent<ChangeTextColor>().ChangeColor();
            PlayerPrefs.SetInt("audio", 1);
            PlayerPrefs.Save();
        }
        else if(PlayerPrefs.GetInt("audio") == 1)
        {
            GameObject.Find("OptionSoundText").GetComponent<ChangeTextColor>().ChangeColor();
            PlayerPrefs.SetInt("audio", 0);
            PlayerPrefs.Save();
        }
    }

    public void showFps()
    {
        if(PlayerPrefs.GetInt("showfps") == 0)
        {
            GameObject.Find("OptionFpsText").GetComponent<ChangeTextColor>().ChangeColor();
            PlayerPrefs.SetInt("showfps", 1);
            PlayerPrefs.Save();
        }
        else if(PlayerPrefs.GetInt("showfps") == 1)
        {
            GameObject.Find("OptionFpsText").GetComponent<ChangeTextColor>().ChangeColor();
            PlayerPrefs.SetInt("showfps", 0);
            PlayerPrefs.Save();
        }
    }

    public void Exit() {
        Application.Quit();
    }
	
}
