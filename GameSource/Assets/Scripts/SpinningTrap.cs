﻿using UnityEngine;
using System.Collections;

public class SpinningTrap : MonoBehaviour {

    float interval;
    bool invoked;
    bool rotate;
    AudioSource audiosource;
    public AudioClip swing;

    void Start () {
        audiosource = gameObject.GetComponent<AudioSource>();
        interval = Random.Range(1.0f, 3.0f);
        invoked = false;
        rotate = true;
    }
	
	void FixedUpdate () {

        if (rotate)
            Rotate();

        if ((int) transform.eulerAngles.y % 90 == 0)
        {
            if (!invoked)
            {
                rotate = false;
                invoked = true;
                Invoke("spin", interval);
            }
            
        }
    }

    void spin()
    {
        rotate = true;
        invoked = false;
    }

    void Rotate()
    {
        audiosource.PlayOneShot(swing);
        transform.Rotate(new Vector3(0, 90, 0) * 5.0f * Time.deltaTime);
    }
}
