﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class EnemyStats : MonoBehaviour {

    GameObject player;
    PlayerStats playerStats;
    Canvas dmgCanvas;
    Animator animator;

    private int level;
    private float hp;

    private int strength;       // atk dmg, 
    private int constitution;   // max hp, hp regen, defence
    private int dexterity;      // movement speed, jump height
    private int luck;
    
    private float atkDmg;
    private float maxHp;
    private float def;
    private float moveSpeed;
    private float critRate;

    private int mapLevel;
    private int scoreToGiveToPlayer;
    private float expToGiveToPlayer;
    private bool takenDmg;
    private bool dying;

    private float alertDist;
    private int behaviour;

    AudioSource audiosource;
    public AudioClip hit;
    public AudioClip die;

    void Awake()
    {
        animator = gameObject.GetComponent<Animator>();
        dying = false;
        alertDist = Random.Range(8.0f, 16.0f);
        behaviour = Random.Range(0, 3);
        audiosource = gameObject.GetComponent<AudioSource>();
    }
    
    void Start () {
        dmgCanvas = gameObject.GetComponentInChildren<Canvas>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerStats = player.GetComponent<PlayerStats>();

        mapLevel = playerStats.GetMapLevel();
        level = Random.Range(mapLevel, mapLevel + 5);
        strength = 5 + Random.Range(level - 5, level + 5);
        constitution = 5 + Random.Range(level - 5, level + 5);
        dexterity = 5 + Random.Range(level - 5, level + 5);
        luck = 5 + Random.Range(level - 5, level + 5);
        RecalcSecondary();
        scoreToGiveToPlayer = level * 10;
        expToGiveToPlayer = level * 5 + playerStats.GetLuck() * Random.Range(1,10);
        takenDmg = false;
        
        
    }

    void Update()
    {
        if(hp < 0.1f && takenDmg)
        {
            if (!dying)
            {
                dying = true;
                Die();
            }
        }
    }

    void RecalcSecondary()
    {
        atkDmg = 1 + strength;

        maxHp = 100 + constitution + strength * 0.15f;
        hp = maxHp;
        def = 1 + constitution;

        if (moveSpeed <= 12.0f)
            moveSpeed = 6 + 0.006f * dexterity;

        if (critRate <= 25.0f)
            critRate = 1 + 0.25f * luck;
    }

    void Die()
    {
        GiveScore();
        GiveExp();
        animator.SetTrigger("Die");
        audiosource.PlayOneShot(die);
        foreach (Canvas c in gameObject.GetComponentsInChildren<Canvas>())
        {
            c.enabled = false;
        }
        //Destroy(gameObject, 3.0f);
    }
    public bool IsDying()
    {
        return dying;
    }

    public void GetDamaged(float x)
    {
        float dmg = x;
        float defence = def + Random.Range(0, constitution / 2);
        dmg -= defence;
        if (dmg > 0.0f)
        {
            audiosource.PlayOneShot(hit);
            takenDmg = true;
            hp -= dmg;
            dmgCanvas.GetComponentInChildren<Text>().text = Mathf.Round(dmg).ToString();
            Invoke("clearDmg", 1.0f);
        }
        else
        {
            dmgCanvas.GetComponentInChildren<Text>().text = "0";
            Invoke("clearDmg", 1.0f);
        }
    }

    void clearDmg()
    {
        dmgCanvas.GetComponentInChildren<Text>().text = "";
    }

    public float DoDamage()
    {
        float dmg = atkDmg + Random.Range(0, strength);
        if (Random.Range(0.0f, 100.0f) < critRate)
            dmg = dmg + dmg * Random.Range(0.5f, 2.0f);
        return dmg;
    }

    public int GetLevel()
    {
        return level;
    }

    public float GetHp()
    {
        return hp;
    }
    public float GetMaxHp()
    {
        return maxHp;
    }
    public float GetAtkDmg()
    {
        return atkDmg;
    }
    public float GetDef()
    {
        return def;
    }
    public float GetMoveSpeed()
    {
        return moveSpeed;
    }

    public void GiveScore()
    {
        playerStats.SetScore(scoreToGiveToPlayer);
    }
    public void GiveExp()
    {
        playerStats.SetExp(playerStats.GetExp() + expToGiveToPlayer);
    }
    public float GetAlertDist()
    {
        return alertDist;
    }
    public int GetBehaviour()
    {
        return behaviour;
    }

}
