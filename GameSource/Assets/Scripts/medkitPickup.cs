﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class medkitPickup : MonoBehaviour {

    PlayerStats player;
    int value;
    bool medkitPickedUp;
    Text txt;
    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        value = Random.Range(1,6) * 10;
        medkitPickedUp = false;
        txt = gameObject.GetComponentInChildren<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        Rotate();
    }

    void Rotate()
    {
        transform.RotateAround(transform.position, transform.up, Time.deltaTime * 50.0f);
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "Player")
        {
            if (!medkitPickedUp)
            {
                player.AddHp(value);
                medkitPickedUp = true;
                txt.text = "+" + value + " hp";
                Destroy(gameObject, 1.2f);
            }

        }
    }
}
