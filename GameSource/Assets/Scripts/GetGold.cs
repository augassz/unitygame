﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GetGold : MonoBehaviour {

    PlayerStats p;

    void Start () {
        p = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        	
	}
	
	// Update is called once per frame
	void Update () {
        gameObject.GetComponent<Text>().text = "Money: " + p.GetGold();

    }
}
