﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapManager : MonoBehaviour {

	private List<GameObject> mapParts = new List<GameObject> ();
	private GameObject player;
	int oldIndex;
    
	private float mapGenX = 0.0f;
	private float mapGenStep = 100.0f;

	private float mapGenY = 0.0f;
	private float mapGenZ = 5.0f;

	GameObject prevMap = null;
	GameObject currMap = null; 
	GameObject nextMap = null;
    GameObject temp = null;

	// Use this for initialization
	void Start () {		
		foreach(GameObject g in Resources.LoadAll("MapPrefabs")){
			mapParts.Add(g);
		}
		player = GameObject.FindGameObjectWithTag ("Player");
		transform.position = new Vector3 (mapGenX, mapGenY, mapGenZ);
        CreateForFistTime();
    }
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x - player.transform.position.x  <= 70.0f) {
			CreateMap ();
            SpawnEnemies();
            DeleteMap();
        }
		
	}

    void SpawnEnemies()
    {
        gameObject.GetComponent<EnemySpawnControl>().Spawn();
    }

	int Rnd(){
		int i = oldIndex;
		while (i == oldIndex) {
			i = Random.Range (0, mapParts.Count);
		}
		oldIndex = i;
		return i;
	}

    void CreateForFistTime() {
        int index = Rnd();
        currMap = (GameObject)Instantiate(mapParts[index], transform.position, transform.rotation);
        transform.position = new Vector3(transform.position.x + mapGenStep, mapGenY, mapGenZ);
        nextMap = (GameObject)Instantiate(mapParts[index], transform.position, transform.rotation);
        transform.position = new Vector3(transform.position.x + mapGenStep, mapGenY, mapGenZ);
    }

	void CreateMap(){
		int index = Rnd();
        prevMap = currMap;
        currMap = nextMap;
        nextMap = (GameObject)Instantiate(mapParts[index], transform.position, transform.rotation);
        transform.position = new Vector3(transform.position.x + mapGenStep, mapGenY, mapGenZ);
    }

	void DeleteMap(){
		if (prevMap != null) { 
			Destroy (prevMap);
			prevMap = null;
		}
	}

}
