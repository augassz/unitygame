﻿using UnityEngine;
using System.Collections;

public class OpenChest : MonoBehaviour {

    public GameObject[] items;
    Animator anim;
    bool called;
    AudioSource audiosource;
    public AudioClip open;
    
	// Use this for initialization
	void Start () {
        audiosource = gameObject.GetComponent<AudioSource>();
        anim = gameObject.GetComponent<Animator>();
        called = false;

    }
	
	// Update is called once per frame
	void Update () {

    }

    void createItem()
    {
        int a = Random.Range(0, items.Length);
        Instantiate(items[a], new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z), transform.rotation);
    }

    void openChest()
    {
        if (!called)
        {
            audiosource.PlayOneShot(open);
            anim.SetTrigger("Open");
            Invoke("createItem", 0.2f);
        }
        called = true;
    }

    public void Open()
    {
        Invoke("openChest", 1.5f);
    }
}
