﻿using UnityEngine;
using System.Collections;

public class bladeDoDmg : MonoBehaviour {

    PlayerStats player;

    float dmg;
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        dmg = Random.Range(player.GetDef(), player.GetDef() + 30.0f);
    }

    // Update is called once per frame
    void Update () {
	
	}

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "Player")
            player.GetDamaged(dmg);
    }
}
