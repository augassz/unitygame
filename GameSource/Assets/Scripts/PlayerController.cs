﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {

    private PlayerStats playerStats;

	public bool keyboardControl = true;
	Vector3 moveVec;
	GameObject player;
	bool canJump;
	float rayCastDis;
    bool canMove;
    bool attackPlaying;
    GameObject attackField;
    float lastY;

    private Animator animator;

	// Use this for initialization
	void Start () {
        player = gameObject;
		rayCastDis = player.transform.position.y + 0.5f;
		canJump = true;
        canMove = true;
        attackPlaying = false;

        playerStats = gameObject.GetComponent<PlayerStats>();

        animator = gameObject.GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void FixedUpdate () {
		
	//	moveVec = new Vector3 (CrossPlatformInputManager.GetAxis ("Horizontal"), 0.0f, CrossPlatformInputManager.GetAxis ("Vertical"));
	//	if (moveVec != Vector3.zero && canMove) {
	//		AnalogMovement ();
		//}
      //  else
        //{
            animator.SetBool("Walk", false);
        //}
        if (keyboardControl)
        {
            if (Input.anyKey)
            {
                if (canMove)
                    KeyboardMovement();
            }
        }

        if (CrossPlatformInputManager.GetButton ("Jump")) {
			Jump ();
		}
		if (CrossPlatformInputManager.GetButton ("Attack")) {
			Attack ();
		}




        lastY = transform.position.y;

    }

	private void AnalogMovement(){
        animator.SetBool("Walk", true);
        player.transform.rotation = Quaternion.LookRotation(new Vector3 (moveVec.x, 0.0f, moveVec.z));
		player.transform.position += transform.forward * Time.deltaTime * playerStats.GetMoveSpeed();
	}
		
	void CheckIfCanJump(){
        if(lastY == transform.position.y)
            canJump = true;
	}

	void Jump(){
		CheckIfCanJump ();
		if (canJump) {
            animator.SetBool("Jump", true);
            player.GetComponent<Rigidbody>().velocity = new Vector3(0, playerStats.GetJumpH(), 0);

            Invoke("StopJump", 0.1f);	
			canJump = false;
		}
	}
    void StopJump()
    {
        animator.SetBool("Jump", false);
    }

	void Attack(){

        if (!attackPlaying)
        {
            attackPlaying = true;
            canMove = false;
             int r = Random.Range(1, 4);
             if(r == 1)
             {
                 animator.SetTrigger("Attack");
             }
             else if(r == 2)
             {
                 animator.SetTrigger("Attack1");
             }
             else if(r == 3)
             {
                 animator.SetTrigger("Attack2");
             }
            Invoke("changeAttackState", 0.5f);
            dodamage();
            
        }
        
    }
    void dodamage()
    {
        Collider c = gameObject.GetComponentInChildren<DetectCollision>().GetColliderEnemy();
        if(c != null)
        {
            if(c.tag == "Enemy")
            {
                c.gameObject.GetComponent<EnemyStats>().GetDamaged(playerStats.DoDamage());
            }
            else if(c.tag == "Chest")
            {
                c.gameObject.GetComponent<OpenChest>().Open();
            }
        }
    }

    void changeAttackState()
    {
        attackPlaying = false;
        Invoke("changeMoveState", 1.0f);
    }
    void changeMoveState()
    {
        
        canMove = true;
    }


    private void KeyboardMovement(){
        if (Input.GetKey(KeyCode.K))
        {
            playerStats.GetDamaged(10000.0f);
        }

        if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A)){
            animator.SetBool("Walk", true);
            player.transform.rotation = Quaternion.LookRotation(new Vector3 (-0.5f, 0.0f, 0.5f));
			player.transform.position += transform.forward * Time.deltaTime * playerStats.GetMoveSpeed();
		}

		else if(Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D)){
            animator.SetBool("Walk", true);
            player.transform.rotation = Quaternion.LookRotation(new Vector3 (0.5f, 0.0f, 0.5f));
			player.transform.position += transform.forward * Time.deltaTime * playerStats.GetMoveSpeed();
		}

		else if(Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D)){
            animator.SetBool("Walk", true);
            player.transform.rotation = Quaternion.LookRotation(new Vector3 (0.5f, 0.0f, -0.5f));
			player.transform.position += transform.forward * Time.deltaTime * playerStats.GetMoveSpeed();
		}

		else if(Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A)){
            animator.SetBool("Walk", true);
            player.transform.rotation = Quaternion.LookRotation(new Vector3 (-0.5f, 0.0f, -0.5f));
			player.transform.position += transform.forward * Time.deltaTime * playerStats.GetMoveSpeed();
		}
		else if(Input.GetKey(KeyCode.A)){
            animator.SetBool("Walk", true);
            player.transform.rotation = Quaternion.LookRotation(new Vector3 (-1.0f, 0.0f, 0.0f));
			player.transform.position += transform.forward * Time.deltaTime * playerStats.GetMoveSpeed();
		}

		else if(Input.GetKey(KeyCode.W)){
            animator.SetBool("Walk", true);
            player.transform.rotation = Quaternion.LookRotation(new Vector3 (0.0f, 0.0f, 1.0f));
			player.transform.position += transform.forward * Time.deltaTime * playerStats.GetMoveSpeed();
		}

		else if(Input.GetKey(KeyCode.D)){
            animator.SetBool("Walk", true);
            player.transform.rotation = Quaternion.LookRotation(new Vector3 (1.0f, 0.0f, 0.0f));
			player.transform.position += transform.forward * Time.deltaTime * playerStats.GetMoveSpeed();
		}

		else if(Input.GetKey(KeyCode.S)){
            animator.SetBool("Walk", true);
            player.transform.rotation = Quaternion.LookRotation(new Vector3 (0.0f, 0.0f, -1.0f));
			player.transform.position += transform.forward * Time.deltaTime * playerStats.GetMoveSpeed();
		}
	}


}