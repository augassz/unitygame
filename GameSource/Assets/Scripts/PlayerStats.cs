﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class PlayerStats : MonoBehaviour {

    GameObject player;
    Canvas dmgCanvas;

    private int score;

    private int level;
    private float exp;
    private float expToNextLvl;
    private int statPoints;

    private float hp;

    private int strength;       // atk dmg, 
    private int constitution;   // max hp, hp regen, defence
    private int dexterity;      // movement speed, jump height
    private int luck;           // drop rate, crit rate, 

    private float atkDmg;

    private float maxHp;
    private float hpRegen;
    private float def;
    
    private float moveSpeed;
    private float jumpHeight;

    private float dropRate;
    private float critRate;

    private int mapLevel;

    private int gold;

    private float lastatk;
    private float lastRegen;
    private bool dying;
    AudioSource audiosource;
    public AudioClip hit;
    public AudioClip die;

    void Awake() {
        audiosource = gameObject.GetComponent<AudioSource>();
        dmgCanvas = gameObject.GetComponentInChildren<Canvas>();
        dying = false;
        if (PlayerPrefs.GetInt("gamestart") == 1) { // Load game

            score = PlayerPrefs.GetInt("score");

            level = PlayerPrefs.GetInt("level");
            exp = PlayerPrefs.GetFloat("exp");
            expToNextLvl = PlayerPrefs.GetFloat("exptonext");
            statPoints = PlayerPrefs.GetInt("statpts");
            SetStr(PlayerPrefs.GetInt("str"));
            SetCons(PlayerPrefs.GetInt("con"));
            SetDex(PlayerPrefs.GetInt("dex"));
            SetLuck(PlayerPrefs.GetInt("lck"));
            hp = PlayerPrefs.GetFloat("hp");
            gold = PlayerPrefs.GetInt("gold");
            mapLevel = PlayerPrefs.GetInt("maplevel");
        }
        else {  // New game

            score = 0;
            level = 1;
            exp = 0;
            expToNextLvl = 100;
            statPoints = 3;
            SetStr(10);
            SetCons(10);
            SetDex(10);
            SetLuck(10);
            hp = maxHp;
            gold = 0;
            mapLevel = 1;
            gold = 0;
        }
        
    }

    // Use this for initialization
    void Start () {
        player = gameObject;
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        if(hp < 0.1f)
        {
            if (!dying)
            {
                dying = true;
                Dead();
            }
            
        }

        if(Time.time >= lastRegen && hp <= maxHp)
        {
            hp += hpRegen;
            lastRegen = Time.time + 1.0f;
        }
	}

    void Dead()
    {
        audiosource.PlayOneShot(die);
        Time.timeScale = 0.0f;
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PauseMenuControl>().GameOver();
    }

    public void GetDamaged(float x)
    {
        float dmg = x;
        float defence = def + Random.Range(0, constitution / 2);
        dmg -= defence;
        if (dmg > 0.0f)
        {
            audiosource.PlayOneShot(hit);
            hp -= dmg;
            dmgCanvas.GetComponentInChildren<Text>().text = Mathf.Round(dmg).ToString();
            Invoke("clearDmg", 1.0f);
        }
        else
        {
            dmgCanvas.GetComponentInChildren<Text>().text = "0";
            Invoke("clearDmg", 1.0f);
        }
    }
    void clearDmg()
    {
        dmgCanvas.GetComponentInChildren<Text>().text = "";
    }

    public float DoDamage()
    {
        float dmg = atkDmg + Random.Range(0, strength);
        if (Random.Range(0.0f, 100.0f) < critRate)
             dmg = dmg + dmg * Random.Range(0.5f, 2.0f);
        return dmg;
    }

    public void SetScore(int x) {
        score += x;
    }

    void LevelUp(float expOver) {
        level += 1;
        statPoints += 1;
        exp = expOver;
        expToNextLvl += 20;
        AddHp(maxHp / 2);
    }
    
    public void SetExp(float x) {
        exp = x;
        if (exp >= expToNextLvl) LevelUp(x - expToNextLvl);
    }

    public void SetStr(int x) {
        strength = x;
        RecalcSecondary();
    }

    public void SetCons(int x) {
        constitution = x;
        RecalcSecondary();
    }

    public void SetDex(int x) {
        dexterity = x;
        RecalcSecondary();
    }

    public void SetLuck(int x) {
        luck = x;
        RecalcSecondary();
    }

    private void RecalcSecondary()
    {
        atkDmg = 10 + strength;

        maxHp = 100 +  constitution + strength * 0.15f;
        hpRegen = 0.01f + constitution * 0.01f;
        def = 10 + constitution;

        if(moveSpeed <= 12.0f)
        moveSpeed = 6 + 0.006f * dexterity;
        if(jumpHeight <= 12.0f)
        jumpHeight = 5 + 0.007f * dexterity;

        dropRate = 1 + 0.25f * luck;
        if (critRate <= 50.0f)
            critRate = 1 + 0.25f * luck;
    }

    public void SetMapLevel(int x) {
        mapLevel += x;
    }

    public void SetStatPTS(int x) {
        if(statPoints != 0)
            statPoints += x;
    }

    public void SetGold(int x)
    {
        gold = x;
    }
    public void AddHp(float x)
    {
        if (hp + x > maxHp)
            hp = maxHp;
        else
            hp += x;
    }
    #region getters
    public int GetScore() {
        return score;
    }

    public int GetLevel() {
        return level;
    }
    public float GetExp() {
        return exp;
    }
    public float GetExpToNext() {
        return expToNextLvl;
    }
    public int GetStatPoints() {
        return statPoints;
    }
    public float GetHp() {
        return hp;
    }
    public int GetStr() {
        return strength;
    }
    public int GetCons() {
        return constitution;
    }
    public int GetDex() {
        return dexterity;
    }
    public int GetLuck() {
        return luck;
    }
    public float GetAtkDmg() {
        return atkDmg;
    }
    public float GetMaxHp() {
        return maxHp;
    }
    public float GetHpRegen() {
        return hpRegen;
    }
    public float GetDef() {
        return def;
    }
    public float GetMoveSpeed() {
        return moveSpeed;
    }
    public float GetJumpH() {
        return jumpHeight;
    }
    public float GetDropRate() {
        return dropRate;
    }
    public float GetCritRate() {
        return critRate;
    }
    public int GetMapLevel() {
        return mapLevel;
    }
    public int GetGold()
    {
        return gold;
    }

    #endregion
}
