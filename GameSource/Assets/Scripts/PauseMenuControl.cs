﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System;

public class PauseMenuControl : MonoBehaviour {

    public Canvas PauseMenuCanvas;
    public Canvas StatsCanvas;
    public Canvas gameOver;
    public Canvas Inventory;

    PlayerStats player;

    bool writen = false;

    void Awake() {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();

        PauseMenuCanvas.enabled = false;
        StatsCanvas.enabled = false;
        gameOver.enabled = false;
        Inventory.enabled = false;
        Time.timeScale = 1;

        
    }

    public void ShowPauseMenu() {
        PauseMenuCanvas.enabled = true;
        Time.timeScale = 0.0f;
    }

    public void GameOver()
    {
        gameOver.enabled = true;
        Time.timeScale = 0.0f;
        GameObject g = GameObject.Find("scoretext");
        g.GetComponent<Text>().text = "Score: " + player.GetScore();

        string highscoreLine = System.DateTime.Now.ToString() + "-" + player.GetScore().ToString();
        StreamWriter writer = new StreamWriter(Application.persistentDataPath + "/Highscores.txt", true);
        if (!writen)
            writer.WriteLine(highscoreLine);
        writen = true;
        writer.Close();

        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetInt("loadgame", 0);
        PlayerPrefs.Save();
    }

    public void ClosePauseMenu() {
        PauseMenuCanvas.enabled = false;
        Time.timeScale = 1;
    }

    public void Stats() {
        StatsCanvas.enabled = true;
        PauseMenuCanvas.enabled = false;
    }

    public void InventoryOpen()
    {
        PauseMenuCanvas.enabled = false;
        Inventory.enabled = true;
    }

    public void Savegame()
    {
        GameObject savegamebtn = GameObject.Find("SaveGame");
        Text svgm = savegamebtn.GetComponentInChildren<Text>();

        PlayerPrefs.SetInt("loadgame", 1);

        PlayerPrefs.SetInt("score", player.GetScore());
        PlayerPrefs.SetInt("level", player.GetLevel());
        PlayerPrefs.SetFloat("exp", player.GetExp());
        PlayerPrefs.SetFloat("exptonext", player.GetExpToNext());
        PlayerPrefs.SetInt("statpts", player.GetStatPoints());
        PlayerPrefs.SetFloat("hp", player.GetHp());
        PlayerPrefs.SetInt("str", player.GetStr());
        PlayerPrefs.SetInt("con", player.GetCons());
        PlayerPrefs.SetInt("dex", player.GetDex());
        PlayerPrefs.SetInt("lck", player.GetLuck());
        PlayerPrefs.SetInt("maplevel", player.GetMapLevel());
        PlayerPrefs.Save();
        svgm.color = Color.green;
        svgm.text = "Game saved";
    }

    public void ReturnToPauseMenu() {
        PauseMenuCanvas.enabled = true;
        StatsCanvas.enabled = false;
        gameOver.enabled = false;
        Inventory.enabled = false;
    }

    public void IncSTR() {
        if(player.GetStatPoints() != 0) { 
            player.SetStr(player.GetStr() + 1);
            player.SetStatPTS(-1);
        }
    }

    public void IncCON() {
        if (player.GetStatPoints() != 0) { 
            player.SetCons(player.GetCons() + 1);
            player.SetStatPTS(-1);
        }
    }

    public void IncDEX(){
        if (player.GetStatPoints() != 0) { 
            player.SetDex(player.GetDex() + 1);
            player.SetStatPTS(-1);
        }
    
    }

    public void IncLUCK() {
        if (player.GetStatPoints() != 0) { 
            player.SetLuck(player.GetLuck() + 1);
            player.SetStatPTS(-1);
        }
    }

    public void ReturnToMenu() {
        SceneManager.LoadScene(0);
    }
}
