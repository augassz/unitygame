﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class EnemySpawnControl : MonoBehaviour {

    private List<GameObject> spawnPoints = new List<GameObject>();
    private List<GameObject> enemies = new List<GameObject>();

    // Use this for initialization
    void Start () {

        foreach (GameObject g in Resources.LoadAll("Enemies"))
        {
            enemies.Add(g);
        }

    }
	
	// Update is called once per frame
	void Update () {
        
    }

    void AddSpawnPoints()
    {
        GameObject[] spawns = GameObject.FindGameObjectsWithTag("EnemySpawnPoint");
        foreach(GameObject g in spawns)
        {
            spawnPoints.Add(g);
        }
    }

    void SpawnEnemey(int where)
    {
        int what = Random.Range(0, enemies.Count);
        Instantiate(enemies[what], spawnPoints[where].transform.position, spawnPoints[where].transform.rotation);
        DestroyImmediate(spawnPoints[where]);
        spawnPoints.RemoveAt(where);
    }

    public void Spawn()
    {
        AddSpawnPoints();
        int howMany = Random.Range(0, spawnPoints.Count) + 1;

        for (int i = 0; i < howMany; i++){
            int where = Random.Range(0, spawnPoints.Count);
            SpawnEnemey(where);
        }

        for (int i = 0; i < spawnPoints.Count; i++)
        {
            DestroyImmediate(spawnPoints[i]);
        }
        spawnPoints.Clear();
    }

}
